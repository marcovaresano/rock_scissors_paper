let closeIcon = document.querySelector(".close-icon");
let rules = document.querySelector(".rules");
let btnRules = document.querySelector(".btn-rules");
let paperBtn = document.querySelector("#paper");
let scissorsBtn = document.querySelector("#scissors");
let rockBtn = document.querySelector("#rock");
let mainGame = document.querySelector(".gameStart");
let gameScreen = document.querySelector(".game-screen");
let btnPlayAgain = document.querySelector(".btn-playagain");
let pickYouWrapper = document.querySelector(".pickYou-wrapper");
let pickHouseWrapper = document.querySelector(".pickHouse-wrapper");
let resultMatch = document.querySelector("#resultMatch");
let scoreUpdate = document.querySelector("#score");
console.log(scoreUpdate);

closeIcon.addEventListener("click", () => {
  rules.classList.add("d-none");
  $(".overlay").fadeOut();
});

btnRules.addEventListener("click", () => {
  $(".overlay").fadeIn().removeClass("d-none");
  rules.classList.toggle("d-none");
});

var userChoice = undefined;
var houseChoice = undefined;

paperBtn.addEventListener("click", () => {
  mainGame.classList.add("main-game");
  gameScreen.classList.remove("d-none");
  userChoice = paperBtn.getAttribute("userchoice");
  PopulateCards();
  getRandomHouse();
  populateHouseCards();
  checkWinner();
  updateScore();
  scoreUpdate.classList.add("animate__backInDown animate_animated");
});

scissorsBtn.addEventListener("click", () => {
  mainGame.classList.add("main-game");
  gameScreen.classList.remove("d-none");
  userChoice = scissorsBtn.getAttribute("userchoice");
  PopulateCards();
  getRandomHouse();
  populateHouseCards();
  checkWinner();
  updateScore();
});
rockBtn.addEventListener("click", () => {
  mainGame.classList.add("main-game");
  gameScreen.classList.remove("d-none");
  userChoice = rockBtn.getAttribute("userchoice");
  PopulateCards();
  getRandomHouse();

  populateHouseCards();
  checkWinner();
  updateScore();
});

btnPlayAgain.addEventListener("click", () => {
  mainGame.classList.remove("main-game");
  gameScreen.classList.add("d-none");
});

function PopulateCards() {
  if (userChoice == 1) {
    populateChoicePaper();
  } else if (userChoice == 2) {
    populateChoiceScissors();
  } else if (userChoice == 3) {
    populateChoiceRock();
  }
}

function populateChoiceScissors() {
  // let choice = document.createElement('div')

  document.querySelector(
    "#userChoice"
  ).innerHTML = `<div class="scissorsgame #animate__bounceIn animate__animated d-flex align-items-center justify-content-center position-absolute">
    <img class="img-fluid" src="./img/icon-scissors.svg" alt="">
    </div>`;

  // choice.innerHTML = '';
  // choice.innerHTML = `
  //      <div class="scissorsgame winner animate__bounceIn animate-animated d-flex align-items-center justify-content-center position-absolute">
  //     <img class="img-fluid" src="./img/icon-scissors.svg" alt="">
  //     </div>
  // `

  // pickYouWrapper.appendChild(choice)
}

function populateChoiceRock() {
  document.querySelector(
    "#userChoice"
  ).innerHTML = `<div class="rockgame #animate__bounceIn animate__animated d-flex align-items-center justify-content-center position-absolute">
        <img class="img-fluid" src="./img/icon-rock.svg" alt="">
        </div>`;

  // pickYouWrapper.appendChild(choice)
}

function populateChoicePaper() {
  // let choice = document.createElement('div')
  document.querySelector(
    "#userChoice"
  ).innerHTML = `<div class="papergame #animate__bounceIn animate__animated d-flex align-items-center justify-content-center position-absolute">
    <img class="img-fluid" src="./img/icon-paper.svg" alt="">
    </div>`;

  // choice.innerHTML = `
  //      <div class="papergame animate__bounceIn animate-animated d-flex align-items-center justify-content-center position-absolute">
  //     <img class="img-fluid" src="./img/icon-paper.svg" alt="">
  //     </div>
  // `
  // pickYouWrapper.appendChild(choice)
}

function getRandomHouse() {
  houseChoice = Math.floor(Math.random() * (3 - 1 + 1) + 1);
}

function populateHouseCards() {
  if (houseChoice == 1) {
    populateChoicePaperHouse();
  } else if (houseChoice == 2) {
    populateChoiceScissorsHouse();
  } else if (houseChoice == 3) {
    populateChoiceRockHouse();
  }
}

function populateChoiceScissorsHouse() {
  let choice = document.createElement("div");
  choice.innerHTML = `
         <div class="scissorsgame houseCard d-flex align-items-center justify-content-center position-absolute">
        <img class="img-fluid" src="./img/icon-scissors.svg" alt="">
        </div>
    `;

  pickHouseWrapper.appendChild(choice);
}

function populateChoiceRockHouse() {
  let choice = document.createElement("div");
  choice.innerHTML = `
         <div class="rockgame houseCard d-flex align-items-center justify-content-center position-absolute">
        <img class="img-fluid" src="./img/icon-rock.svg" alt="">
        </div>
    `;

  pickHouseWrapper.appendChild(choice);
}

function populateChoicePaperHouse() {
  let choice = document.createElement("div");
  choice.innerHTML = `
         <div class="papergame houseCard d-flex align-items-center justify-content-center position-absolute">
        <img class="img-fluid" src="./img/icon-paper.svg" alt="">
        </div>
    `;
  pickHouseWrapper.appendChild(choice);
}

var result = 0;

function checkWinner() {
  if (userChoice == houseChoice) {
    result = 0;
    resultMatch.innerHTML = "DRAW";
  } else if (userChoice == 1 && houseChoice == 2) {
    result = 2;
    resultMatch.innerHTML = "YOU LOOSE";
  } else if (userChoice == 2 && houseChoice == 1) {
    result = 1;
    resultMatch.innerHTML = "YOU WIN";
  } else if (userChoice == 3 && houseChoice == 2) {
    result = 1;
    resultMatch.innerHTML = "YOU WIN";
  } else if (userChoice == 3 && houseChoice == 1) {
    result = 2;
    resultMatch.innerHTML = "YOU LOOSE";
  } else if (userChoice == 2 && houseChoice == 3) {
    result = 2;
    resultMatch.innerHTML = "YOU LOOSE";
  } else if (userChoice == 1 && houseChoice == 3) {
    result = 1;
    resultMatch.innerHTML = "YOU WIN";
  }
}

var score = 0;

function updateScore() {
  if (result === 1) {
    score = score + 1;
    scoreUpdate.innerHTML = `${score}`;
  } else if (result === 2) {
    score = score - 1;
    scoreUpdate.innerHTML = `${score}`;
  }
  return score;
}
